;  __     __   __     __     ______   ______     __        
; /\ \   /\ "-.\ \   /\ \   /\__  _\ /\  ___\   /\ \       
; \ \ \  \ \ \-.  \  \ \ \  \/_/\ \/ \ \  __\   \ \ \____  
;  \ \_\  \ \_\\"\_\  \ \_\    \ \_\  \ \_____\  \ \_____\ 
;   \/_/   \/_/ \/_/   \/_/     \/_/   \/_____/   \/_____/ 
 

;;; Bad stuff
(setq package-check-signature nil)

;;; Making the interface not terrible ;;;
(menu-bar-mode -1)


;;; Sane defaults ;;;
(setq-default word-wrap t)              ; wrap lines
(setq delete-old-versions -1)		; delete excess backup versions silently
(setq version-control t)		; use version control
(setq vc-make-backup-files t)		; make backups file even when in version controlled dir
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")) ) ; which directory to put backups file
(setq vc-follow-symlinks t)				       ; don't ask for confirmation when opening symlinked file
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)) ) ;transform backups file name
(setq inhibit-startup-screen t)	; inhibit useless and old-school startup screen
(setq ring-bell-function 'ignore)	; silent bell when you make a mistake
(setq coding-system-for-read 'utf-8 )	; use utf-8 by default
(setq coding-system-for-write 'utf-8 )
(set-language-environment "UTF-8")
(setq sentence-end-double-space nil)	; sentence SHOULD end with only a point.
(setq default-fill-column 80)		; toggle wrapping text at the 80th character
(setq-default indent-tabs-mode nil)     ; tabs to spaces
(global-set-key "\C-m" 'newline-and-indent)
;; Scratch Buffer
;; (setq initial-scratch-message
;;       (format
;;        ";; %s\n\n"
;;        (replace-regexp-in-string
;;         "\n" "\n;; " ; comment each line
;;         (replace-regexp-in-string
;;          "\n$" ""    ; remove trailing linebreak
;;          (shell-command-to-string "toilet -d ~/labs/figlet-fonts -f Sub-Zero Im\ Gay")))))


;;; Setup Recent Files
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)


;;; Use package ;;;
(setq package-enable-at-startup nil) ; tells emacs not to load any packages before starting up
;; the following lines tell emacs where on the internet to look up
;; for new packages.
(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
                         ("gnu"       . "http://elpa.gnu.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")))
(package-initialize) ; guess what this one does ?

; Bootstrap `use-package'
(unless (package-installed-p 'use-package) ; unless it is already installed
  (package-refresh-contents) ; updage packages archive
  (package-install 'use-package)) ; and install the most recent version of use-package

(require 'use-package) ; guess what this one does too ?
(setq use-package-always-ensure t) ; autodownload plugins
(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
    (auto-package-update-maybe))

;;; Evil!!

; General
(use-package general
  :config
  (setq general-default-prefix "<SPC>")
  (general-evil-setup))

; Undo-tree
(use-package undo-tree)

;goto-chg
(use-package goto-chg)

; evil-mode
(use-package evil
  :init
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (setq evil-mode-line-format '(before . mode-line-front-space))
  (setq-default display-line-numbers 'visual
    display-line-numbers-widen t
    ;; this is the default
    display-line-numbers-current-absolute t)
  (defun noct:relative ()
    (setq-local display-line-numbers 'visual))
  (defun noct:absolute ()
    (setq-local display-line-numbers t))
  (add-hook 'evil-insert-state-entry-hook #'noct:absolute)
  (add-hook 'evil-insert-state-exit-hook #'noct:relative)
  (custom-set-faces '(line-number ((t :foreground "brightblack"))))
  (custom-set-faces '(line-number-current-line ((t :weight bold
                                                   :foreground "white")))))

; evil-escape
(use-package evil-escape
  :config
  (setq-default evil-escape-key-sequence "jk")
  (evil-escape-mode))

;  evil- commentary
(use-package evil-commentary
  :config
  (evil-commentary-mode))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "e4c8810d9ab925567a69c11d5c95d198a4e7d05871453b2c92c020712559c4c1" default)))
 '(package-selected-packages
   (quote
    (scel sclang w3m auctex-latexmk company-auctex company-math magit telephone-line focus latex-preview-pane tex auctex org-bullets syndicate evil-org smartparens-config smartparens flx counsel swiper ivy xresources-theme evil-commentary evil-escape evil undo-tree general use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(line-number ((t :foreground "brightblack")))
 '(line-number-current-line ((t :weight bold :foreground "white"))))

;;; Editing Packages

; focus-mode
(use-package focus
  :commands focus-mode)

(use-package writeroom-mode
  :commands writeroom-mode)

; flyspell

(use-package flyspell
  :after (:any tex org)
  :config
  (setq ispell-program-name "aspell")
  ;; Please note ispell-extra-args contains ACTUAL parameters passed to aspell
    (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US")))

; company-mode
(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))
(use-package pos-tip
  :after compmay)
(use-package company-quickhelp
  :after company
  :config (company-quickhelp-mode))


;;; Evil!!

; General
(use-package general
  :config
  (setq general-default-prefix "<SPC>")
  (general-evil-setup))

; Undo-tree
(use-package undo-tree)

;goto-chg
(use-package goto-chg)

; evil-mode
(use-package evil
  :init
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (setq evil-mode-line-format '(before . mode-line-front-space))
  (setq-default display-line-numbers 'visual
    display-line-numbers-widen t
    ;; this is the default
    display-line-numbers-current-absolute t)
  (defun noct:relative ()
    (setq-local display-line-numbers 'visual))
  (defun noct:absolute ()
    (setq-local display-line-numbers t))
  (add-hook 'evil-insert-state-entry-hook #'noct:absolute)
  (add-hook 'evil-insert-state-exit-hook #'noct:relative)
  (custom-set-faces '(line-number ((t :foreground "brightblack"))))
  (custom-set-faces '(line-number-current-line ((t :weight bold
                                                   :foreground "white")))))

; evil-escape
(use-package evil-escape
  :config
  (setq-default evil-escape-key-sequence "jk")
  (evil-escape-mode))

;  evil- commentary
(use-package evil-commentary
  :config
  (evil-commentary-mode))


;;; Interface

;theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'xresources)

; fix those pesky highligts
(set-face-attribute 'region nil :background "brightblack")

; rich-minority
(use-package rich-minority
  :config
  (setq rm-whitelist "*")
  (rich-minority-mode 1))

; smart-mode-line
(use-package smart-mode-line
  :config
  (setq sml/theme 'respectful)
  (sml/setup))

;; ivy

;flx
(use-package flx)

;ivy
(use-package ivy
  :config
  (setq ivy-initial-inputs-alist nil)
  (setq ivy-re-builders-alist
    '((ivy-switch-buffer . ivy--regex-plus)
        (t . ivy--regex-fuzzy)))
  (ivy-mode 1))

;swiper
(use-package swiper
  :after ivy)

;counsel
(use-package counsel
  :after ivy
  :general
  (general-nmap
    ":" 'counsel-M-x))

; Parens
(show-paren-mode 1)
(set-face-background 'show-paren-match "brightblack")
    (set-face-attribute 'show-paren-match nil :weight 'bold)
(use-package smartparens
  :config
  (require 'smartparens-config)
  (smartparens-global-mode 1))


;;; Org Mode

; org
(use-package org
  :mode ("\\.org\\'" . org-mode)
  :interpreter ("org" . org-mode))

; evil-org
(use-package evil-org
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme '(textobjects insert navigation additional shift todo heading))))
  (require 'evil-org-agenda)
    (evil-org-agenda-set-keys))

;;; LaTeX

(use-package tex
  :ensure auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
  :interpreter ("tex" . LaTeX-mode)
  :config
  (setq TeX-save-query nil)
  (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
  (setq TeX-source-correlate-mode t)
  (setq LaTeX-item-indent 1)
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (TeX-engine-set "luatex")
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'TeX-interactive-mode)
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
  (add-to-list 'TeX-view-program-selection
             '(output-pdf "Zathura"))
  (setq reftex-plug-into-AUCTeX t))

(use-package company-auctex
    :after tex
    :config
    (company-auctex-init))

(use-package company-math
    :after tex
    :config
    (defun my-latex-mode-setup ()
        (setq-local company-backends
                    (append '((company-math-symbols-latex company-math-symbols-unicode company-latex-commands company-auctex))
                            company-backends)))
    (add-hook 'TeX-mode-hook 'my-latex-mode-setup))

;;; git

; magit
(use-package magit)


;;; Misc
(use-package w3m)

(add-to-list 'load-path "~/.emacs.d/scel/el")
(require 'sclang)
